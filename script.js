function randomMinMax(min, max){
  let rand = min + Math.random() * ( max + 1 - min );
  return Math.floor(rand);
}

function getRandomStroke(n){
  let newStroke = '';

  for ( let i = 0; i < n; i++ ) {
    if (Math.round(Math.random()) === 0) {
      newStroke += String.fromCodePoint(randomMinMax(65, 90));
    } else {
      newStroke += String.fromCodePoint(randomMinMax(48, 57));
    }
  }

  return newStroke;
}

function changeSymbols(stroke, symbol){
  let newStroke = '';
  for ( let i = 0; i < stroke.length; i++ ) {
    if (( stroke.codePointAt(i) >= 65 ) && ( stroke.codePointAt(i) <= 90 )) {
      newStroke += symbol;
    } else {
      newStroke += stroke[i];
    }
  }

  return newStroke;
}

function changeNumbers(stroke, symbol){

  let changeStroke = '';
  for ( let i = 0; i < stroke.length; i++ ) {
    if (( stroke.codePointAt(i) >= 48 ) && ( stroke.codePointAt(i) <= 57 )) {
      changeStroke += symbol;
    } else {
      changeStroke += stroke[i];
    }
  }

  return changeStroke;
}

function sumSymbol(stroke, symbol){
  let sum = 0;
  for ( let char of stroke ) {
    if (char === symbol) {
      sum++;
    }
  }

  return sum;
}


function unit(){
  let length = document.getElementById("inputRandomStroke").value;
  let firstSymbol = document.getElementById("inputFirstSymbol").value;
  let secondSymbol = document.getElementById("inputSecondSymbol").value;

  if (length === '' || firstSymbol === '' || secondSymbol === '') {
    alert("Заполните все поля");
    return;
  }

  if (!isFinite(length)) {
    alert("Длинна строки должна быть числом");
    return;
  }

  if (firstSymbol === secondSymbol) {
    alert("Первый и второй символы не должны быть равны");
    return;
  }

  let stroke = getRandomStroke(length);
  document.getElementById("randomStroke").value = stroke;
  stroke = changeSymbols(stroke, firstSymbol);
  stroke = changeNumbers(stroke, secondSymbol);

  document.getElementById("changeStroke").value = stroke;

  document.getElementById("sumFirstSymbol").innerHTML = "Повторений симола "
    + firstSymbol + ": " + sumSymbol(stroke, firstSymbol);
  document.getElementById("sumSecondSymbol").innerHTML = "Повторений симола "
    + secondSymbol + ": " + sumSymbol(stroke, secondSymbol);

  document.getElementById("outputDiv").classList.remove("hide");
}
